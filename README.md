## unlock_code_galaxy ##




This program was only tested with Samsung Galaxy Ace, although it may also work with Galaxy Mini and others.

The method used is the same as [this](http://forum.xda-developers.com/showthread.php?t=1335548) page, you only need to extract bml5 from your mobile phone and place it in the same folder of the compiled **unlock.cpp**.


Basically I just wanted to make things easier for people who are not so comfortable with computers. This way the only thing you need is a rooted phone and the program will do the rest for you. 
Just don't forget that in order to enter the *unlock code* you must have the stock ROM installed, custom ROM's like CM7 remove this pop-up window.


* * *


## Useful codes ##


```

*#7465625# - Dial code to check if phone it's network locked.
#7465625*638*UNLOCK-CODE# - Replace UNLOCK-CODE with your 8 digits number, then dial. Phone might reboot.

```